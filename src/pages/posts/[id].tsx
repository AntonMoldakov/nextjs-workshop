import { Post } from 'components';
import { apiUrl } from 'constant';
import { MainLayout } from 'layouts';
import { useRouter } from 'next/router';
import React, { useEffect, useState } from 'react';
import { PostType } from 'types';

const PostPage = () => {
  const [post, setPost] = useState<null | PostType>(null);
  const router = useRouter();
  const id = router.query.id;

  const getPosts = async () => {
    try {
      const data = await fetch(`${apiUrl}/posts/${id}`);
      const post = (await data.json()) as PostType;

      setPost(post);
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    getPosts();
  }, []);

  return (
    <MainLayout isLoading={!post}>
      <Post withNavigate={false} post={post!} />
    </MainLayout>
  );
};

export default PostPage;
