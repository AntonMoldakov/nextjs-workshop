import React, { useEffect, useState } from 'react';
import { MainLayout } from 'layouts';
import { apiUrl } from 'constant';
import { PostType } from 'types';
import { Post } from 'components';
import { Grid } from '@mui/material';

const PostsPage = () => {
  const [posts, setPosts] = useState<PostType[]>([]);

  const getPosts = async () => {
    try {
      const data = await fetch(`${apiUrl}/posts`);
      const posts = (await data.json()) as PostType[];

      setPosts(posts);
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    getPosts();
  }, []);

  return (
    <MainLayout isLoading={!posts.length}>
      <Grid container justifyContent="center" alignContent="center" spacing={5}>
        {posts.map((post) => (
          <Grid item spacing={3} key={post.id}>
            <Post post={post} />
          </Grid>
        ))}
      </Grid>
    </MainLayout>
  );
};

export default PostsPage;
