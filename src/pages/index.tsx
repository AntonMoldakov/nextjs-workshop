import { MainLayout } from 'layouts';
import type { NextPage } from 'next';
import Head from 'next/head';
import Image from 'next/image';

const Home: NextPage = () => {
  return <MainLayout>HOME</MainLayout>;
};

export default Home;
