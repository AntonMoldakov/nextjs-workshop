import {
  Card,
  CardActionArea,
  CardContent,
  CardMedia,
  Typography,
} from '@mui/material';
import { RoutePaths } from 'constant';
import { useRouter } from 'next/router';
import React from 'react';
import { PostType } from 'types';

const Post = ({ post, withNavigate = true }: PostProps) => {
  const router = useRouter();

  const navigate = () =>
    withNavigate && router.push(`${RoutePaths.posts}/${post.id}`);

  return (
    <Card onClick={navigate} sx={{ maxWidth: 345 }}>
      <CardActionArea>
        <CardMedia
          component="img"
          height="140"
          image={post.image}
          alt="green iguana"
        />
        <CardContent>
          <Typography gutterBottom variant="h5" component="div">
            {post.title}
          </Typography>
          <Typography variant="body2" color="text.secondary">
            {post.author}
          </Typography>
        </CardContent>
      </CardActionArea>
    </Card>
  );
};

export default Post;

type PostProps = {
  post: PostType;
  withNavigate?: boolean;
};
