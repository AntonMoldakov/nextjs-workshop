import React, { useMemo } from 'react';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import MaterialLink from '@mui/material/Link';
import Button from '@mui/material/Button';
import IconButton from '@mui/material/IconButton';
import Link from 'next/link';
import { RoutePaths } from 'constant';
import styled from 'styled-components';

const Header = () => {
  const pathLinks = useMemo(
    () => [
      { title: 'Главня', href: RoutePaths.home },
      { title: 'Посты', href: RoutePaths.posts },
    ],
    []
  );

  return (
    <Box sx={{ flexGrow: 1 }}>
      <AppBar position="static">
        <StyledToolbar>
          {pathLinks.map((link, index) => (
            <Link key={index} href={link.href} passHref>
              <MaterialLink variant="h6" color="white" underline="hover">
                {link.title}
              </MaterialLink>
            </Link>
          ))}
          {/* <Button color="inherit">Login</Button> */}
        </StyledToolbar>
      </AppBar>
    </Box>
  );
};

export default Header;

const StyledToolbar = styled(Toolbar)`
  gap: 24px;
`;
