import { NextPage } from 'next';
import React from 'react';
import { Header } from 'components';
import styled from 'styled-components';
import { CircularProgress } from '@mui/material';

const MainLayout: NextPage<MainLayoutProps> = ({
  children,
  isLoading = false,
}) => {
  return (
    <div>
      <Header />
      {isLoading ? (
        <LoaderContainer>
          <CircularProgress />
        </LoaderContainer>
      ) : (
        <Main>{children}</Main>
      )}
    </div>
  );
};

export default MainLayout;

type MainLayoutProps = {
  isLoading?: boolean;
};

const Main = styled.main`
  padding: 24px 50px;
`;

const LoaderContainer = styled.div`
  position: absolute;
  display: flex;
  width: 100%;
  height: 100vh;
  justify-content: center;
  align-items: center;
`;
