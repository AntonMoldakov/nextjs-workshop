export type PostType = {
  id: string;
  title: string;
  author: string;
  image: string;
};
