This is the nextjs-workshop project created by purrweb.

## Getting Started

First, run the api and the development server:

```bash
npm run start:dev
# or
yarn start:dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the development server.

Open [http://localhost:5000](http://localhost:3000) with your browser to see the api.
